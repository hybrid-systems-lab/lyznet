System dynamics: x' =  [x2, -0.5*x2 - sin(x1 + 1.0471975511966) + 0.866025403784439]
Domain:  [[-2.0, 3.0], [-3.0, 1.5]]
__________________________________________________
Verifying local stability using quadratic Lyapunov function:
x^TPx =  (2 * (x1 * x2) + 2 * pow(x1, 2) + 3 * pow(x2, 2))
Quadratic Lyapunov function can't verify global stability.
Verifying local stability...
Largest level set x^T*P*x <= 0.05083531141281128 verified by linearization.
Time taken for verification: 0.09308409690856934 seconds.

__________________________________________________
Verifying ROA using quadratic Lyapunov function:
x^TPx =  (2 * (x1 * x2) + 2 * pow(x1, 2) + 3 * pow(x2, 2))
Largest level set x^T*P*x <= 1.0403633117675781 verified by reach & stay.
Time taken for verification: 0.23465180397033691 seconds.

__________________________________________________
Generating training data from numerical integration:
Generating new training data...
100%|███████████████████████████████████████████████████████████████| 3000/3000 [00:59<00:00, 50.69it/s]
Saving training data...
Time taken for generating data: 60.60479426383972 seconds.

__________________________________________________
Learning neural Lyapunov function:
Training model...
Epoch 1 completed. Average epoch loss: 0.004995. Max epoch loss: 0.51995                                
Epoch 2 completed. Average epoch loss: 0.00018673. Max epoch loss: 0.00036046                           
Epoch 3 completed. Average epoch loss: 8.9359e-05. Max epoch loss: 0.00020276                           
Epoch 4 completed. Average epoch loss: 4.677e-05. Max epoch loss: 0.0001314                             
Epoch 5 completed. Average epoch loss: 2.9514e-05. Max epoch loss: 9.7169e-05                           
Epoch 6 completed. Average epoch loss: 2.1265e-05. Max epoch loss: 7.9242e-05                           
Epoch 7 completed. Average epoch loss: 1.6305e-05. Max epoch loss: 6.6206e-05                           
Epoch 8 completed. Average epoch loss: 1.3209e-05. Max epoch loss: 7.4272e-05                           
Epoch 9 completed. Average epoch loss: 1.1136e-05. Max epoch loss: 5.6549e-05                           
Epoch 10 completed. Average epoch loss: 9.7157e-06. Max epoch loss: 5.8527e-05                          
Epoch 11 completed. Average epoch loss: 8.6023e-06. Max epoch loss: 4.8643e-05                          
Epoch 12 completed. Average epoch loss: 7.7844e-06. Max epoch loss: 4.4992e-05                          
Epoch 13 completed. Average epoch loss: 7.0486e-06. Max epoch loss: 3.8776e-05                          
Epoch 14 completed. Average epoch loss: 6.4508e-06. Max epoch loss: 2.6797e-05                          
Epoch 15 completed. Average epoch loss: 5.9426e-06. Max epoch loss: 3.3574e-05                          
Epoch 16 completed. Average epoch loss: 5.484e-06. Max epoch loss: 3.3382e-05                           
Epoch 17 completed. Average epoch loss: 5.0758e-06. Max epoch loss: 2.6755e-05                          
Epoch 18 completed. Average epoch loss: 4.7092e-06. Max epoch loss: 2.4819e-05                          
Epoch 19 completed. Average epoch loss: 4.4131e-06. Max epoch loss: 2.9487e-05                          
Epoch 20 completed. Average epoch loss: 4.1385e-06. Max epoch loss: 2.1263e-05                          
Total training time: 325.94 seconds.
Model trained: results/ex2_two_machine_power_data_only_loss=Data_N=300000_epoch=20_layer=2_width=30_lr=0.001_data=3000.pt
__________________________________________________
Verifying neural Lyapunov function:
Verified V<=0.078125 is contained in x^TPx<=1.0403633117675781.
Verified V<=0.3203125 will reach V<=0.078125 and hence x^TPx<=1.0403633117675781.
Time taken for verifying Lyapunov function of ex2_two_machine_power_data_only: 2168.0697400569916 seconds.

__________________________________________________
Verifying SOS Lyapunov function:
V =  ( - 2.92715e-06 * x1 + 3.57113e-05 * x2 + 0.383702 * (x1 * x2) + 1.24138 * (x1 * pow(x2, 2)) + 0.937248 * (x1 * pow(x2, 3)) + 1.35209 * (x1 * pow(x2, 4)) - 2.66463 * (x1 * pow(x2, 5)) + 1.30361 * (pow(x1, 2) * x2) + 5.2694 * (pow(x1, 2) * pow(x2, 2)) - 9.40986 * (pow(x1, 2) * pow(x2, 3)) + 9.61729 * (pow(x1, 2) * pow(x2, 4)) + 3.43975 * (pow(x1, 3) * x2) - 6.42685 * (pow(x1, 3) * pow(x2, 2)) + 5.2509 * (pow(x1, 3) * pow(x2, 3)) + 0.813029 * (pow(x1, 4) * x2) + 0.502922 * (pow(x1, 4) * pow(x2, 2)) - 2.65909 * (pow(x1, 5) * x2) + 0.224015 * pow(x1, 2) + 0.239486 * pow(x1, 3) + 1.61831 * pow(x1, 4) - 2.11898 * pow(x1, 5) + 1.20704 * pow(x1, 6) + 0.647698 * pow(x2, 2) + 1.4535 * pow(x2, 3) + 1.70048 * pow(x2, 4) - 4.16795 * pow(x2, 5) + 2.91229 * pow(x2, 6))
Verified V<=0.1171875 is contained in x^TPx<=1.0403633117675781.
Verified V<=1 will reach V<=0.1171875 and hence x^TPx<=1.0403633117675781.
Time taken for verifying Lyapunov function of ex2_two_machine_power_data_only: 1.8763179779052734 seconds.

__________________________________________________
Generating training data from numerical integration:
Generating new training data...
100%|█████████████████████████████████████████████████████████████| 90000/90000 [26:49<00:00, 55.92it/s]
Saving training data...
Time taken for generating data: 1610.904510974884 seconds.

__________________________________________________
Testing volume of sublevel set of neural Lyapunov function...
The size of data is 90000
The size of ROA (approximated by data) is 39760
The approximate volume ratio of verified ROA is 39.12%
__________________________________________________
Testing volume of sublevel set of SOS Lyapunov function...
The size of data is 90000
The size of ROA (approximated by data) is 39760
The approximate volume ratio of verified ROA is 18.53%