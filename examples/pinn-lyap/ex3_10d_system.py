import sympy as sp
import lyznet

lyznet.utils.set_random_seed()

x1, x2, x3, x4, x5, x6, x7, x8, x9, x10 = sp.symbols('x1:11')

f = [
    -x1 + 0.5 * x2 - 0.1 * x9**2,
    -0.5 * x1 - x2,
    -x3 + 0.5 * x4 - 0.1 * x1**2,
    -0.5 * x3 - x4,
    -x5 + 0.5 * x6 + 0.1 * x7**2,
    -0.5 * x5 - x6,
    -x7 + 0.5 * x8,
    -0.5 * x7 - x8,
    -x9 + 0.5 * x10,
    -0.5 * x9 - x10 + 0.1 * x2**2
    ]

domain = [[-1, 1]] * 10
sys_name = "ex3_10d_system"
system = lyznet.DynamicalSystem(f, domain, sys_name)

print("System dynamics: x' = ", system.symbolic_f)

c1_P = lyznet.local_stability_verifier(system)


net, model_path = lyznet.neural_learner(system, lr=0.001, 
                                        num_colloc_pts=300000, max_epoch=20, 
                                        layer=1, width=10, 
                                        loss_mode="Lyapunov", 
                                        net_type="Simple")


# # Call the neural lyapunov verifier
# c1_V, c2_V = lyznet.neural_verifier(system, net, 0.2, net_type="Simple")

c1_V = 0.0390625
c2_V = 0.09375

lyznet.plot_V(system, net, model_path, c2_V=c2_V, 
              c2_P=0.49999356269836426, phase_portrait=True, 
              lie_derivative=True)
