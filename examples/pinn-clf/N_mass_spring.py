import sympy as sp
import lyznet
import numpy as np

# Number of masses
N = 6

# Define symbolic variables x1 to x20
symbols = sp.symbols(f'x1:{2*N+1}')  # Generates x1, x2, ..., x20

f = []
for i in range(1, N + 1):
    pos = 2 * i - 1  # Position index (x1, x3, ..., x19)
    vel = 2 * i      # Velocity index (x2, x4, ..., x20)
    
    # Position derivative: dx_pos/dt = vel
    f.append(symbols[vel - 1])
    
    # Velocity derivative: dv_i/dt = F_i (forces acting on mass i)
    if i == 1:
        # First mass: attached to a fixed wall on the left with a nonlinear spring
        # and connected to the second mass on the right with a spring and damper
        F_left = -symbols[pos - 1] - (1/10)*symbols[pos - 1]**3  # Nonlinear spring
        F_right_spring = -(symbols[pos - 1] - symbols[pos + 1])   # Spring to mass 2
        F_right_damper = (1/10)*(symbols[vel - 1] - symbols[vel + 1])  # Damper to mass 2
        F_total = F_left + F_right_spring + F_right_damper
        f.append(F_total)
    elif i < N:
        # Intermediate masses: connected to both left and right neighbors with springs and dampers
        F_left_spring = -(symbols[pos - 1] - symbols[pos - 3])    # Spring to left mass
        F_left_damper = (1/10)*(symbols[vel - 1] - symbols[vel - 3])  # Damper to left mass
        F_right_spring = -(symbols[pos - 1] - symbols[pos + 1])   # Spring to right mass
        F_right_damper = (1/10)*(symbols[vel - 1] - symbols[vel + 1])  # Damper to right mass
        F_total = F_left_spring + F_left_damper + F_right_spring + F_right_damper
        f.append(F_total)
    else:
        # Last mass: only connected to the left neighbor with a spring and damper
        F_left_spring = -(symbols[pos - 1] - symbols[pos - 3])    # Spring to left mass
        F_left_damper = (1/10)*(symbols[vel - 1] - symbols[vel - 3])  # Damper to left mass
        F_total = F_left_spring + F_left_damper
        f.append(F_total)

# Define the control input matrix
g = [0] * (2 * N)
g[1] = 1  # Control input affects the velocity of the first mass (v1)
g_matrix = sp.Matrix(g)

# Define the domain for each variable
domain = [[-10, 10]] * (2 * N)

# Name of the system
sys_name = f"mass_spring_global_clf_{2*N}D"

# Create the control affine system
f_matrix = sp.Matrix(f)
system = lyznet.ControlAffineSystem(f_matrix, g_matrix, domain, sys_name)

print("Dynamics: ", system.symbolic_f)

lyznet.z3_global_quadratic_clf_verifier(system)
