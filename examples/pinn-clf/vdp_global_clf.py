import sympy as sp
import numpy as np
import lyznet

lyznet.utils.set_random_seed(seed=123)

# Define dynamics
mu = 1.0
x1, x2 = sp.symbols('x1 x2')
f = sp.Matrix([x2, -x1 + mu * (1 - x1**2) * x2])
g = sp.Matrix([0, 1.0])

domain = [[-4.0, 4.0]] * 2

sys_name = f"vdp_global_clf"
system = lyznet.ControlAffineSystem(f, g, domain, sys_name)

lyznet.z3_global_quadratic_clf_verifier(system)

# # code to plot trajectories and control inputs of the closed-loop system 
# # under Sontag's controller if needed
# x = sp.Matrix(system.symbolic_vars)    
# V = x.T * sp.Matrix(system.P) * x
# u_func_numpy = lyznet.utils.u_func_numpy_from_sontag(system, V)
# system.closed_loop_f_numpy = lyznet.get_closed_loop_f_numpy(
#     system, system.f_numpy_vectorized, system.g_numpy_vectorized, u_func_numpy
# )
# lyznet.plot_V(system, 
# 			  phase_portrait=True,               
# 			  plot_trajectories=True,
#               plot_control=True,
#               plot_cost=True,
#               u_func=u_func_numpy
#               )
