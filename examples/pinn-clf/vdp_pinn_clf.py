import sympy as sp
import numpy as np
import lyznet
import torch

lyznet.utils.set_random_seed(seed=123)

x1, x2 = sp.symbols('x1 x2')
f = sp.Matrix([-x2, x1 + (x1**2 - 1) * x2])
g = sp.Matrix([0, (x1**2 - 1) * x2])

# Compute the Jacobian of f(x)
def jacobian_f(x):
    x1 = x[0, :]
    x2 = x[1, :]
    df_dx = np.zeros((2, 2, x.shape[1]))
    
    # Partial derivatives of f1 = -x2
    df_dx[0, 1, :] = -1  # ∂f1/∂x2 = -1
    
    # Partial derivatives of f2 = x1 + (x1^2 - 1) * x2
    df_dx[1, 0, :] = 1 + 2 * x1 * x2  # ∂f2/∂x1 = 1 + 2 * x1 * x2
    df_dx[1, 1, :] = x1**2 - 1  # ∂f2/∂x2 = x1^2 - 1
    
    return df_dx

# Compute the Jacobian of g(x)
def jacobian_g(x):
    x1 = x[0, :]
    x2 = x[1, :]
    dg_dx = np.zeros((2, 1, 2, x.shape[1]))  
    
    # Partial derivatives of g1 = 0 (all zeros)
    
    # Partial derivatives of g2 = (x1^2 - 1) * x2
    dg_dx[1, 0, 0, :] = 2 * x1 * x2  # ∂g2/∂x1 = 2 * x1 * x2
    dg_dx[1, 0, 1, :] = x1**2 - 1  # ∂g2/∂x2 = x1^2 - 1
    
    return dg_dx


def g_torch(x):
    dx1 = torch.zeros_like(x[:, 0])  # 0 for all rows
    dx2 = (x[:, 0]**2 - 1) * x[:, 1]  # (x1^2 - 1) * x2 for all rows

    return torch.stack([dx1, dx2], dim=1).unsqueeze(-1)


domain = [[-4, 4]] * 2 # domain for data generation 

sys_name = f"vdp_pinn_clf_{domain[0]}"
system = lyznet.ControlAffineSystem(f, g, domain, sys_name)

lyznet.z3_global_quadratic_clf_verifier(system)
c2_P = lyznet.z3_quadratic_clf_verifier(system) 

data = lyznet.generate_pmp_data(system, n_samples=3000, 
								df_x_numpy=jacobian_f,
								dg_x_numpy=jacobian_g
								)

system.domain = [[-18, 18]] * 2 # domain for training
test = [[-8, 8]] * 2
system.name = f"vdp_pinn_clf_{test[0]}"

# switch loss_mode to 'Data' for comparison 
net, model_path = lyznet.neural_learner(system, 
										data=data, 
										lr=0.001, layer=2, 
                                        width=20, num_colloc_pts=300000, 
                                        g_torch=g_torch,
                                        max_epoch=5,
                                        loss_mode="Zubov_Control",
                                        # loss_mode="Data", 
                                        )


c1_V, c2_V = lyznet.neural_clf_verifier(system, net, c2_P)

# 2x30 with pinn on [-8,8]; verified CLF level
# c2_V = 0.8333740234375

# 2x30 with data only on [-8,8]; verified CLF level
# c2_V = 0.4937

lyznet.plot_V(system, net, model_path, c2_P=c2_P, c2_V=c2_V)


# # to verify closed-loop system stability under near-optimal controller
# closed_loop_sys = lyznet.utils.closed_loop_system_from_Zubov_HJB(net, system)

# c1_P = lyznet.local_stability_verifier(closed_loop_sys)
# c2_P = lyznet.quadratic_reach_verifier(closed_loop_sys, c1_P)
# c1_V, c2_V = lyznet.neural_verifier(closed_loop_sys, net, c2_P)


# # to compare Sontag and near-optimal controller
# # Sontag controller
# u_func_numpy = lyznet.utils.u_func_numpy_from_sontag_torch(net, system, g_torch=g_torch)

# # # HJB controller
# u_func_numpy = lyznet.utils.u_func_numpy_from_Zubov_HJB_torch(net, system, g_torch=g_torch)

# system.closed_loop_f_numpy = lyznet.get_closed_loop_f_numpy(
#     system, system.f_numpy_vectorized, system.g_numpy_vectorized, u_func_numpy
# )


# lyznet.plot_V(system, net, model_path, c2_P=c2_P, c2_V=c2_V, 
#               phase_portrait=True,
#               # plot_trajectories=True,
#               # plot_control=True,
#               plot_cost=True,
#               u_func=u_func_numpy
#               )


# # plot level set comparison with rational/SOS CLF

# def rational_V(x1, x2):
#     numerator = (
#         1.5 * x1**2 + 0.0394355 * x1**4 - x1 * x2
#         + 0.451638 * x1**3 * x2 + x2**2 + 0.0856408 * x1**2 * x2**2
#         - 0.0132402 * x1 * x2**3 - 0.0290605 * x2**4
#     )
#     denominator = (
#         1 + 0.023885 * x1**2 + 0.317015 * x1 * x2 - 0.0735786 * x2**2
#     )
#     return numerator / denominator

# c2_rational = 2.65


# def sos_V(x1, x2):
#     return (
#         1.49634937086e-21 * x1
#         + 3.57940116523e-22 * x2
#         + 1.19640764383 * x1**2
#         + 5.73522219449e-19 * x1**2 * x2
#         - 0.0718937073772 * x1 * x2
#         + 1.19439036549 * x2**2
#         + 0.000837003112278 * x1**3 * x2
#         - 0.000115118536129 * x1**2 * x2**2
#         + 8.80315379019e-19 * x1 * x2**2
#         + 3.40281066395e-19 * x1**3
#         + 2.30662760302e-19 * x2**3
#         - 0.000231555960957 * x1**4
#         - 0.000747846013315 * x1 * x2**3
#         - 0.000265596926721 * x2**4
#         - 3.62394776376e-22 * x1**5
#         + 5.14365013952e-22 * x1**4 * x2
#         - 4.1860926793e-21 * x1**3 * x2**2
#         + 4.03401269138e-21 * x1**2 * x2**3
#         + 2.69859741885e-21 * x1 * x2**4
#         + 1.64020175692e-22 * x2**5
#         + 1.19956077853e-08 * x1**6
#         - 5.50756428056e-08 * x1**5 * x2
#         - 6.99960628831e-10 * x1**4 * x2**2
#         + 2.18720571834e-08 * x1**3 * x2**3
#         + 1.38699722331e-08 * x1**2 * x2**4
#         + 2.98576575285e-08 * x1 * x2**5
#         + 1.97480384769e-08 * x2**6
#     )


# c2_SOS = 1

# # compare quadratic CLF with rational and SOS
# lyznet.plot_V(system, 
#               V_list=[sos_V, rational_V], c_lists=[[c2_SOS], [2.65]], 
#               c2_P=c2_P,
#               # phase_portrait=True
#               )


# # compare rational, SOS, quadratic, and neural CLFs
# lyznet.plot_V(system, net, model_path, 
#               V_list=[sos_V, rational_V], c_lists=[[c2_SOS], [2.65]], 
#               c2_V=c2_V, c2_P=c2_P,
#               # phase_portrait=True
#               )

# # plot verified quadratic and neural CLF only
# lyznet.plot_V(system, net, model_path, c2_P=c2_P, c2_V=c2_V)
