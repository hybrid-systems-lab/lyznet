import sympy as sp
import lyznet

lyznet.utils.set_random_seed(123)

x1, x2 = sp.symbols('x1 x2')
symbolic_vars = (x1, x2)

g_c = 9.81
m = 0.15
l = 0.5
b = 0.1

f = sp.Matrix([x2, g_c/l*sp.sin(x1) - b/(m*l**2)*x2])
g = sp.Matrix([0, 1/(m*l**2)])

domain = [[-10, 10]] * 2
sys_name = f"pendulum_global_qclf"
system = lyznet.ControlAffineSystem(f, g, domain, sys_name)

lyznet.z3_global_quadratic_clf_verifier(system)
