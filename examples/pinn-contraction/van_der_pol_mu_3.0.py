import sympy 
import lyznet
import torch

lyznet.utils.set_random_seed()

# Define dynamics
mu = 3.0
x1, x2 = sympy.symbols('x1 x2')
f_vdp = [-x2, x1 - mu * (1 - x1**2) * x2]

domain_vdp = [[-3.0, 3.0], [-6.0, 6.0]]

sys_name = f"van_der_pol_mu_{mu}"
vdp_system = lyznet.DynamicalSystem(f_vdp, domain_vdp, sys_name)

print("System dynamics: x' = ", vdp_system.symbolic_f)
print("Domain: ", vdp_system.domain)

# Call the local stability verifier
c1_P = lyznet.local_stability_verifier(vdp_system)
# Call the quadratic verifier
c2_P = lyznet.quadratic_reach_verifier(vdp_system, c1_P)

# Generate data (needed for data-augmented learner)
data = lyznet.generate_data(vdp_system, n_samples=3000)

# Call the neural lyapunov learner
V_net, model_path = lyznet.neural_learner(vdp_system, data=data, lr=0.001, 
                                          layer=2, width=30, 
                                          num_colloc_pts=300000, max_epoch=20,
                                          loss_mode="Zubov")

# # Call the neural lyapunov verifier 
# c1_V, c2_V = lyznet.neural_verifier(vdp_system, V_net, c2_P)

# c2_V = 0.640625 # the largest level verified for ROA
c2_V = 0.4  # largest level on which a CM is learned and verified 


def f_torch(x):
    # x is a tensor of shape (N, d)
    # Return a tensor of shape (N, d)
    x1 = x[:, 0]
    x2 = x[:, 1]

    return torch.stack(
        [-x2,
         x1 - mu * (1 - x1**2) * x2], dim=1)


CM_data = lyznet.generate_CM_data(vdp_system, n_samples=30000)


def filter_converging_trajectories(CM_data, V_net, c2_V):
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    x_data = CM_data[:, :2]
    y_data = CM_data[:, 2:]
    x_data_tensor = torch.FloatTensor(x_data).to(device)

    with torch.no_grad():
        V_values = V_net(x_data_tensor).squeeze()

    mask = V_values.cpu().numpy() <= c2_V
    filtered_CM_data = CM_data[mask]
    return filtered_CM_data


filtered_CM_data = filter_converging_trajectories(CM_data, V_net, c2_V)
filtered_CM_data = filtered_CM_data[:9000]
print("CM_data points used:", filtered_CM_data.shape)

vdp_system.name = f"{vdp_system.name}_V<={c2_V}"


# Call the neural contraction metric learner
M_net, model_path = lyznet.neural_learner(vdp_system, lr=0.001, 
                                          layer=2, width=40, 
                                          f_torch=f_torch, 
                                          # data=filtered_CM_data,
                                          batch_size=512,
                                          num_colloc_pts=1200000, 
                                          max_epoch=400,
                                          loss_mode="Contraction_EP",
                                          # loss_mode="Contraction_EP_PDE",
                                          V_net=V_net, c2_V=c2_V,
                                          )

# uncomment to verify (we verified for c2_V=0.42 for CM using PDI loss 
#                      without the positive definite loss) 

# result = lyznet.neural_CM_verifier(vdp_system, M_net, V_net=V_net, c2_V=c2_V)

# if result is None: 
#     lyznet.plot_V(vdp_system, V_net, model_path, c2_V=c2_V, 
#                   phase_portrait=True)


# Compare verified ROA with SOS
def sos_V(x1, x2):
    return (-3.45170890476e-09*x1 + 1.18207436789e-09*x2 + 0.724166734819*x1**2 
            - 1.76558340904e-09*x1**2*x2 - 0.443120764123*x1*x2 
            + 0.100535553877*x2**2 - 0.0827925966603*x1**3*x2 
            + 0.113707192696*x1**2*x2**2 + 2.66977190991e-09*x1**3 
            - 1.96231083293e-09*x1*x2**2 + 2.46445852241e-09*x2**3 
            - 0.258471270203*x1**4 - 7.15465083743e-05*x1*x2**3 
            - 0.00868754707599*x2**4 - 7.06729359872e-10*x1**5 
            + 4.71809154412e-10*x1**4*x2 + 1.78626846573e-09*x1**3*x2**2 
            - 3.85499453562e-09*x1**2*x2**3 + 2.13640045899e-09*x1*x2**4
            - 6.48146577731e-10*x2**5 + 0.042169106804*x1**6 
            + 0.0659159674928*x1**5*x2 + 0.00064555582031*x1**4*x2**2
            - 0.0243141499204*x1**3*x2**3 + 0.0191263911108*x1**2*x2**4 
            - 0.00770278152905*x1*x2**5 + 0.00153689109462*x2**6)


c2_SOS = 0.72  # computed with Matlab (Yalmip + mosek)

# verify quadratic contraction metric
c2_P_CM = lyznet.quadratic_CM_verifier(vdp_system, c1_P, c2_P)

lyznet.plot_V(vdp_system, V_net, model_path, 
              V_list=[sos_V], c_lists=[[c2_SOS]], c2_V=c2_V, c2_P=c2_P_CM,
              phase_portrait=True)
