import sympy 
import lyznet
import torch

lyznet.utils.set_random_seed()

# Define dynamics
x1, x2 = sympy.symbols('x1 x2')
f = [x2, -2.0*x1 + 1.0/3.0*x1**3 - x2]

# Define domain as a list of [min, max] for each dimension
domain = [[-6.0, 6.0], [-6.0, 6.0]]
sys_name = "polynomial_2d"
system = lyznet.DynamicalSystem(f, domain, sys_name)

print("System dynamics: x' = ", system.symbolic_f)
# print(system.symbolic_vars)

# Call the local stability verifier
c1_P = lyznet.local_stability_verifier(system)
# Call the quadratic verifier
c2_P = lyznet.quadratic_reach_verifier(system, c1_P)

# Generate data (needed for data-augmented learner)
data = lyznet.generate_data(system, n_samples=3000)

# Call the neural lyapunov learner
V_net, model_path = lyznet.neural_learner(system, data=data, lr=0.001, layer=2, 
                                          width=30, num_colloc_pts=300000, 
                                          max_epoch=20, loss_mode="Zubov")

# Call the neural lyapunov verifier
# c1_V, c2_V = lyznet.neural_verifier(system, V_net, c2_P)

# c2_V = 0.9285888671875  # the largest level verified for ROA
c2_V = 0.76  # the largest level on which a CM is learned and verified


def f_torch(x):
    # x is a tensor of shape (N, d)
    x1 = x[:, 0]
    x2 = x[:, 1]

    return torch.stack(
        [x2,
         -2.0*x1 + 1.0/3.0*x1**3 - x2], dim=1)


CM_data = lyznet.generate_CM_data(system, n_samples=30000)


def filter_converging_trajectories(CM_data, V_net, c2_V):
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    x_data = CM_data[:, :2]
    y_data = CM_data[:, 2:]
    x_data_tensor = torch.FloatTensor(x_data).to(device)

    with torch.no_grad():
        V_values = V_net(x_data_tensor).squeeze()

    mask = V_values.cpu().numpy() <= c2_V
    filtered_CM_data = CM_data[mask]
    return filtered_CM_data


filtered_CM_data = filter_converging_trajectories(CM_data, V_net, c2_V)
filtered_CM_data = filtered_CM_data[:9000]
print("CM_data points available:", filtered_CM_data.shape)

system.name = f"{system.name}_V<={c2_V}"

# Call the neural contraction metric learner
M_net, model_path = lyznet.neural_learner(system, lr=0.001, 
                                          layer=2, width=30, 
                                          f_torch=f_torch, 
                                          batch_size=512,
                                          # data=filtered_CM_data,
                                          num_colloc_pts=1200000, 
                                          max_epoch=400,
                                          loss_mode="Contraction_EP", 
                                          # loss_mode="Contraction_EP_PDE",
                                          V_net=V_net, c2_V=c2_V)


# # uncomment to verify (we verified for c2_V=0.76) 
# result = lyznet.neural_CM_verifier(system, M_net, V_net=V_net, c2_V=c2_V)

# if result is None: 
#     lyznet.plot_V(system, V_net, model_path, c2_V=c2_V, 
#                   phase_portrait=True)


def sos_V(x1, x2):
    return (
        -3.01872837285e-14*x1 - 3.138238632e-14*x2 - 1.87625274909e-12*x1**3 + 0.23880537218*x1**2 
        + 0.154735909745*x1*x2 + 0.106942100257*x2**2 - 0.0488646164163*x1**4 - 0.0182450621683*x1**3*x2 
        + 3.886957977e-12*x1**2*x2 - 8.24921729902e-12*x1*x2**2 - 6.06769702089e-12*x2**3 
        - 0.0121291797154*x1**2*x2**2 + 0.00991041619384*x1*x2**3 - 0.00251760989863*x2**4 
        + 3.54064884962e-13*x1**5 - 5.98429793273e-13*x1**4*x2 + 2.10807810804e-12*x1**3*x2**2 
        - 2.03887533095e-12*x1**2*x2**3 + 4.59480258263e-12*x1*x2**4 + 2.07281049712e-12*x2**5 
        + 0.00619782516367*x1**6 + 0.00542527761642*x1**5*x2 + 0.002753717824*x1**4*x2**2 
        - 0.00754133971232*x1**3*x2**3 + 0.00258082909368*x1**2*x2**4 - 0.00384727354101*x1*x2**5 
        + 0.00253364825967*x2**6)


c2_SOS = 0.84  # computed with Matlab (Yalmip + mosek)

# verify quadratic contraction metric
c2_P_CM = lyznet.quadratic_CM_verifier(system, c1_P, c2_P)

lyznet.plot_V(system, V_net, model_path, 
              V_list=[sos_V], c_lists=[[c2_SOS]], c2_V=c2_V, c2_P=c2_P_CM,
              phase_portrait=True)
