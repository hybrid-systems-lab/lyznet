import sympy as sp
import lyznet

lyznet.utils.set_random_seed()

x1, x2 = sp.symbols('x1 x2')
symbolic_vars = (x1, x2)

f = [x2, 19.6*sp.sin(x1) - 4.0*x2]
g = [0, 40.0]

# domain = [[-4, 4]] * 2
# sys_name = f"pendulum_{domain[0]}"


domain = [[-2, 2]] * 2
sys_name = "pendulum"

R = 2.0*sp.eye(1)

initial_u = sp.Matrix([-(1/40)*x1 - (19.6/40)*sp.sin(x1)])

system = lyznet.ControlAffineSystem(f, g, domain, sys_name, R=R)

lyznet.neural_pi(system, initial_u=initial_u, 
                 num_of_iters=10, lr=0.001, layer=2, width=10, 
                 num_colloc_pts=300000, max_epoch=10)
